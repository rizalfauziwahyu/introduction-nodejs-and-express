const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.send('Hello from Routes Folder!');
});

router.get('/home', (req, res, next) => {
    res.render('home', null)
});

router.get('/json', (req, res, next) => {
    const data = {
        error: null,
        status: "200 OK",
        data: [
            "success"
        ]
    }
    res.json(data);
});

module.exports = router